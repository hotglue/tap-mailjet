"""Stream type classes for tap-mailjet."""

from pathlib import Path
from typing import Any, Dict, Optional, Union, List, Iterable

from singer_sdk import typing as th  # JSON Schema typing helpers

from tap_mailjet.client import MailjetStream

# TODO: Delete this is if not using json files for schema definition
SCHEMAS_DIR = Path(__file__).parent / Path("./schemas")
# TODO: - Override `UsersStream` and `GroupsStream` with your own stream definition.
#       - Copy-paste as many times as needed to create multiple stream types.


class RecipientsStream(MailjetStream):
    """Define custom stream."""
    name = "listrecipient"
    path = "/listrecipient"
    primary_keys = ["ID"]
    replication_key = "SubscribedAt"
    # Optionally, you may also use `schema_filepath` in place of `schema`:
    # schema_filepath = SCHEMAS_DIR / "users.json"
    schema = th.PropertiesList(
        th.Property("ContactID", th.NumberType),
        th.Property("ID", th.NumberType),
        th.Property("IsActive", th.BooleanType),
        th.Property("IsUnsubscribed", th.BooleanType),
        th.Property("ListID", th.NumberType),
        th.Property("ListName", th.StringType),
        th.Property("SubscribedAt", th.DateTimeType),
        th.Property("UnsubscribedAt", th.DateTimeType),
    ).to_dict()

class ContactStream(MailjetStream):
    """Define custom stream."""
    name = "contact"
    path = "/contact"
    primary_keys = ["ID"]
    replication_key = "CreatedAt"
    # Optionally, you may also use `schema_filepath` in place of `schema`:
    # schema_filepath = SCHEMAS_DIR / "users.json"
    schema = th.PropertiesList(
        th.Property("ID", th.NumberType),
        th.Property("CreatedAt", th.DateTimeType),
        th.Property("DeliveredCount", th.NumberType),
        th.Property("Email", th.StringType),
        th.Property("ExclusionFromCampaignsUpdatedAt", th.StringType),
        
        th.Property("IsExcludedFromCampaigns", th.BooleanType),
        th.Property("IsOptInPending", th.BooleanType),
        th.Property("IsSpamComplaining", th.BooleanType),
        th.Property("LastActivityAt", th.DateTimeType),
        th.Property("LastUpdateAt", th.DateTimeType),
        th.Property("Name", th.StringType),
        th.Property("UnsubscribedAt", th.DateTimeType),
        th.Property("UnsubscribedBy", th.StringType),
    ).to_dict()


